pub use super::block::Block;
use std::fmt;

#[derive(Default, Debug)]
pub struct Blocks {
    items: Vec<Block>,
}

impl fmt::Display for Blocks {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "blocks: [\n").unwrap();
        for item in &self.items {
            write!(f, "\t{:?}\n", item).unwrap();
        }
        write!(f, "]\n").unwrap();
        Ok(())
    }
}

impl Blocks {
    ///
    /// Parse blocks from vector.
    ///
    /// CAUTION. Will remove values from
    /// original vector.
    ///
    pub fn parse(source: &mut Vec<String>) -> Blocks {
        let mut blocks = Blocks::default();
        while source.len() > 0 {
            let mut _pair = Block::parse(source);
            for _ in 0.._pair.1 {
                source.remove(0);
            }
            if _pair.0.is_some() {
                blocks.items.push(_pair.0.unwrap());
            }
        }
        blocks
    }

    // TODO: REMOVE IT WHEN YOU HAVE ATLEAST ONE USEAGE
    #[allow(dead_code)]
    pub fn count(&self) -> usize {
        self.items.len()
    }

    pub fn block(&self, index: usize) -> &Block {
        &self.items[index]
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parsing_blocks() {
        let mut lines: Vec<String> = vec![
            "/// This is start of block".to_owned(),
            "/// and this is its".to_owned(),
            "/// description".to_owned(),
            "This is not block".to_owned(),
            "/// But This is another block".to_owned(),
            "/// With its own description. ".to_owned(),
            "But it's not reachable yet".to_owned(),
        ];

        let blocks = Blocks::parse(&mut lines);

        assert_eq!(blocks.count(), 2);
    }
}
