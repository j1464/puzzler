use std::fmt;

use super::blocks::Blocks;
use super::ticket::Ticket;

#[derive(Default, Debug)]
pub struct Tickets {
    items: Vec<Ticket>,
}

impl Tickets {
    pub fn parse(blocks: &Blocks) -> Tickets {
        let mut tickets: Vec<Ticket> = vec![];
        for i in 0..blocks.count() {
            // let block = ;
            tickets.push(Ticket::parse(blocks.block(i)));
        }
        Tickets { items: tickets }
    }

    #[allow(dead_code)]
    pub fn len(&self) -> usize {
        self.items.len()
    }
}

impl fmt::Display for Tickets {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "tickets = [\n").unwrap();
        for t in &self.items {
            write!(f, "\t{}\n", t).unwrap();
        }
        write!(f, "]\n")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::blocks::Blocks;

    #[test]
    fn parse_many_blocks_to_tickets() {
        let mut lines: Vec<String> = vec![
            "/// This is start of block".to_owned(),
            "/// ".to_owned(),
            "asdasd".to_owned(),
            "asdasd".to_owned(),
            "asdasd".to_owned(),
            "/// This is start of second block".to_owned(),
            "/// my description".to_owned(),
        ];

        let blocks = Blocks::parse(&mut lines);
        let tickets: Tickets = Tickets::parse(&blocks);

        assert_eq!(tickets.len(), 2);
    }
}
