use clap::Parser;
use std::fmt;

///
/// Configuration for puzzler application.
/// Parsing implemented via clap crate.
/// How it works see here https://docs.rs/clap/3.0.5/clap/
///
#[derive(Parser, Debug, Default)]
#[clap(about, version, author)]
pub struct Config {
    /// Name of the person to greet
    #[clap(short, long)]
    pub filepath: String,
}

///
/// Display trait implementation for Config.
/// Displaying only one filepath.
///
impl fmt::Display for Config {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.filepath)
    }
}
