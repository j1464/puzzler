use super::block::Block;

use std::fmt;

#[derive(Default, Debug)]
pub struct Ticket {
    name: String,
    description: String,
}

impl Ticket {
    pub fn parse(block: &Block) -> Self {
        let mut ticket = Ticket::default();
        ticket.name = block.line(0);

        let mut buffer = String::new();
        for i in 1..block.len() {
            // todo: why this can't add another string ???
            let s = block.line(i);
            if s.len() > 0 {
                buffer += s.as_str();
                if i != block.len() - 1 {
                    buffer.push(' ');
                }
            }
        }
        ticket.description = buffer;
        ticket
    }
}

impl fmt::Display for Ticket {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "name = '{}', description = '{}'",
            self.name, self.description
        )
    }
}

#[cfg(test)]
mod tests {
    use crate::block::Block;

    use super::*;
    #[test]
    fn construct_ticket_from_block() {
        let mut lines: Vec<String> = vec![
            "/// This is start of block".to_owned(),
            "/// ".to_owned(),
            "/// And this is its".to_owned(),
            "/// description".to_owned(),
        ];

        let block = Block::parse(&mut lines);
        let ticket = Ticket::parse(&block.0.unwrap());

        assert_eq!(ticket.name, "This is start of block".to_owned());
        assert_eq!(ticket.description, "And this is its description".to_owned());
    }
}
