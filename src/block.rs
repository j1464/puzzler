///
/// Block of lines from source code wich was
/// surounded by comments. This blocks is
/// may be possible ticket's for Source Code tracking
/// system like Github or Gitlab or some thing linke that
///
#[derive(Default, Debug)]
pub struct Block {
    lines: Vec<String>,
}

impl Block {
    /// Parsing from vector of strings
    /// block.
    /// Parsing only first occurence.
    /// After successfull parsing will return count of parsed lines.
    /// There fore you should remove thouse first count lines for
    /// parsing next block if you think vector can contain it.
    pub fn parse(source: &mut Vec<String>) -> (Option<Block>, usize) {
        let mut count = 0;
        let mut block = Block::default();
        let mut found = false;
        for v in source {
            if v.contains("///") {
                found = true;
                block.lines.push(v[3..].trim().to_owned());
            } else {
                if found {
                    break;
                }
            }
            count += 1;
        }
        if found {
            (Some(block), count)
        } else {
            (None, count)
        }
    }

    pub fn line(&self, index: usize) -> String {
        self.lines[index].clone()
    }

    pub fn len(&self) -> usize {
        self.lines.len()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn read_one_block_from_lines() {
        let mut lines: Vec<String> = vec![
            "some text".to_owned(),
            "/// This is start of block".to_owned(),
            "/// and this is its".to_owned(),
            "/// description".to_owned(),
            "This is not block".to_owned(),
            "/// But This is another block".to_owned(),
            "/// With its own description. ".to_owned(),
            "But it's not reachable yet".to_owned(),
        ];

        let block_pair = Block::parse(&mut lines);
        let block_lines = block_pair.0.unwrap().lines;
        assert_eq!(block_pair.1, 4);
        assert_eq!(block_lines.len(), 3);
        assert_eq!(block_lines[0], "This is start of block".to_owned());
        assert_eq!(block_lines[1], "and this is its".to_owned());
        assert_eq!(block_lines[2], "description".to_owned());
    }

    #[test]
    fn read_two_blocks_from_lines() {
        let mut lines: Vec<String> = vec![
            "/// This is start of block".to_owned(),
            "/// and this is its".to_owned(),
            "/// description".to_owned(),
            "This is not block".to_owned(),
            "/// But This is another block".to_owned(),
            "/// With its own description. ".to_owned(),
            "But it's not reachable yet".to_owned(),
        ];

        let first_block_pair = Block::parse(&mut lines);
        for _ in 0..first_block_pair.1 {
            lines.remove(0);
        }

        assert_eq!(lines.len(), 4);
        assert_eq!(lines[0], "This is not block".to_owned());
        assert_eq!(lines[1], "/// But This is another block".to_owned());
        assert_eq!(lines[2], "/// With its own description. ".to_owned());
        assert_eq!(lines[3], "But it's not reachable yet".to_owned());
        {
            let block_lines = first_block_pair.0.unwrap().lines;
            assert_eq!(first_block_pair.1, 3);
            assert_eq!(block_lines.len(), 3);
            assert_eq!(block_lines[0], "This is start of block".to_owned());
            assert_eq!(block_lines[1], "and this is its".to_owned());
            assert_eq!(block_lines[2], "description".to_owned());
        }
        let second_block_pair = Block::parse(&mut lines);
        {
            let block_lines = second_block_pair.0.unwrap().lines;
            assert_eq!(second_block_pair.1, 3);
            assert_eq!(block_lines.len(), 2);
            assert_eq!(block_lines[0], "But This is another block".to_owned());
            assert_eq!(block_lines[1], "With its own description.".to_owned());
        }
    }
}
