mod block;
mod blocks;
mod config;
mod ticket;
mod tickets;

use config::Config;
use tickets::Tickets;
use blocks::Blocks;
use clap::Parser;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::path::Path;

// TODO: #3 Move this function to another module outside of main

/// Reading all lines from text file at path
/// Path can be anything that implements trait AsRef<Path>
pub fn read_lines<P: AsRef<Path>>(path: P) -> Vec<String> {
    // TODO: #1 Add handling bad file open in read_line function.
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    let mut lines: Vec<String> = Vec::new();
    for line in reader.lines() {
        // TODO: #2 For read_lines function. Add handling Result type here.
        lines.push(line.unwrap());
    }
    lines
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Write;

    #[test]
    fn test_read_lines() {
        let mut file = File::create("temp.txt").unwrap();
        file.write_all(
            b"/// test\n\
            asdas\n\
            asdasdas\n",
        )
        .unwrap();

        // read lines
        let lines = read_lines("temp.txt");
        assert_eq!(lines.len(), 3);
        assert_eq!(lines[0], "/// test".to_owned());
        assert_eq!(lines[1], "asdas".to_owned());
        assert_eq!(lines[2], "asdasdas".to_owned());

        // check correctnes
        std::fs::remove_file("temp.txt").unwrap();
    }
}

fn main() {
    let args = Config::parse();
    let mut lines = read_lines(args.filepath);
    // TODO: #4 Add for Blocks read method wich can read from one 
    // file or many (from folder as example).
    let blocks = Blocks::parse(&mut lines);
    let tickets = Tickets::parse(&blocks);
    println!("{}", tickets);
}
